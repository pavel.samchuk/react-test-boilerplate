import { MainView } from 'routes/main/MainView'
import { ToDoView } from 'routes/todo/ToDoView'

export const routes = [
  {
    path: '/',
    component: MainView,
    exact: true
  },
  {
    path: '/todos',
    component: ToDoView
  }
]