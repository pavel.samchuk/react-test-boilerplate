import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { store } from 'store'
import { routes } from 'routes'
import './index.css'
import registerServiceWorker from './registerServiceWorker'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'



const App = () => {
  return (
    <MuiThemeProvider>
      <Provider store={store}>
        <Router>
          <React.Fragment>
            {
              routes.map((routeProps) => (
                <Route key={routeProps.path} {...routeProps} />
              ))
            }
          </React.Fragment>
        </Router>
      </Provider>
    </MuiThemeProvider>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)

registerServiceWorker()
