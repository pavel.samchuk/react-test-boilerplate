import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import * as todos from 'ducks/todo'
import * as loading from 'ducks/loading'
import * as errors from 'ducks/errors'

const middleware = [thunk]

export const store = createStore(
  combineReducers({
    rootReducer: (state = {}) => (state),
    todosReducer: todos.reducer,
    loadingReducer: loading.reducer,
    errorsReducer: errors.reducer
  }),
  applyMiddleware(...middleware)
)