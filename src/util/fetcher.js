import 'whatwg-fetch'
import { LOADING_STARTED } from 'ducks/loading'
import { NETWORK_ERROR } from 'ducks/errors'

const onSuccess = async (dispatch, result) => {
  const data = await result.json()

  return { data }
}

const onError = (dispatch, result) => {
  const error = { message: result.statusText, status: result.status }

  dispatch({
    type: NETWORK_ERROR,
    error
  })

  return { error }
}

export const fetcher = async (dispatch, url, params = {}) => {
  dispatch({ type: LOADING_STARTED })
  const result = await fetch(url)

  if (result.status.toString().match(/^20\d/)) {
    return await onSuccess(dispatch, result)
  }

  return onError(dispatch, result)
}