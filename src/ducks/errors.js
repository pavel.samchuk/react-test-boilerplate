export const NETWORK_ERROR = 'NETWORK_ERROR'
const CLEAR_ERROR = 'CLEAR_ERROR'

const initialState = {
  errors: {},
  counter: 0
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
  case CLEAR_ERROR:
    const { errors: { [action.errorKey]: errorToRemove, ...rest } } = state

    return {
      counter: state.counter - 1,
      errors: {
        ...rest
      }
    }
  default:
    const counter = state.counter + 1

    return {
      errors: {
        ...state.errors,
        [counter]: action.error
      },
      counter
    }
  }
}