import { fetcher } from 'util/fetcher'
import { TODOS_URL } from 'const/rest'

export const FETCH_TODOS = 'FETCH_TODOS'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
export const FETCH_TODOS_FAIL = 'FETCH_TODOS_FAIL'

export const fetchTodos = () => async (dispatch) => {
  const { data, error } = await fetcher(dispatch, TODOS_URL)

  if (!error) {
    dispatch({
      type: FETCH_TODOS_SUCCESS,
      data
    })
  }
}

export const reducer = (state = {}, action) => {
  switch (action.type) {
  case FETCH_TODOS_SUCCESS:
    const newState = {
      ...state,
      data: [...action.data]
    }

    return newState

  default:
    return state
  }
}