export const LOADING_STARTED = 'LOADING_STARTED'
export const LOADING_COMPLETED = 'LOADING_COMPLETED'

export const reducer = (state = { loaded: false }, action) => {
  switch (action.type) {
  case LOADING_COMPLETED:
    return { loaded: true }
  case LOADING_STARTED:
    return { loaded: false }
  default:
    return state
  }
}